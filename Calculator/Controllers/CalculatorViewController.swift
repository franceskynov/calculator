//
//  CalculatorViewController.swift
//  Calculator
//
//  Created by Dev on 10/26/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class CalculatorViewController: UIViewController {

    
    @IBOutlet weak var operationsLabel: UILabel!
    @IBOutlet weak var firstNumber: UILabel!
    @IBOutlet weak var secondNumber: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    var nmbrPosFlag: Bool? = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func numericKeyboard(_ sender: UIButton) {
        
        if nmbrPosFlag ?? true {
            firstNumber.text = firstNumber.text! + String(sender.tag)
        } else {
            secondNumber.text = secondNumber.text! + String(sender.tag)
        }
    }
    
    @IBAction func operateSymbol(_ sender: UIButton) {
        
        switch sender.tag {
            
        case 11:
            operationsLabel.text = "/"
            nmbrPosFlag = false
            break
        case 12:
            operationsLabel.text = "*"
            nmbrPosFlag = false
            break
        case 13:
            operationsLabel.text = "-"
            nmbrPosFlag = false
            break
        case 14:
            operationsLabel.text = "+"
            nmbrPosFlag = false
            break
        case 15:
            operationsLabel.text = "%"
            nmbrPosFlag = false
            break
        case 16:
            operationsLabel.text = "±"
            nmbrPosFlag = false
            break
            
        default:
            operationsLabel.text = ""
        }
    }
    
    
    @IBAction func work(_ sender: UIButton) {
        
        switch operationsLabel.text {
        case "/":
            resultLabel.text = Util.to_s(n: Calculator.div(a: Util.toNumbr(l: firstNumber), b: Util.toNumbr(l: secondNumber)))
            break
            
        case "*":
            resultLabel.text = Util.to_s(n: Calculator.mult(a: Util.toNumbr(l: firstNumber), b: Util.toNumbr(l: secondNumber)))
            break
    
        case "-":
            resultLabel.text = Util.to_s(n: Calculator.res(a: Util.toNumbr(l: firstNumber), b: Util.toNumbr(l: secondNumber)))
            break
        
        case "+":
            resultLabel.text = Util.to_s(n: Calculator.sum(a: Util.toNumbr(l: firstNumber), b: Util.toNumbr(l: secondNumber)))
            break
            
        case "%":
            resultLabel.text = Util.to_s(n: Calculator.percentage(a: Util.toNumbr(l: firstNumber), b: Util.toNumbr(l: secondNumber)))
            break
            
        case "±":
            resultLabel.text = Util.to_s(n: Calculator.inverse(a: Util.toNumbr(l: firstNumber)) )
            break
        
        default:
            print("---")
        }
        
        nmbrPosFlag = true
    }
    
    
    @IBAction func clean(_ sender: UIButton) {
        nmbrPosFlag = true
        firstNumber.text = ""
        secondNumber.text = ""
        resultLabel.text = ""
        operationsLabel.text = ""
    }
    
}
