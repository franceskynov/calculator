//
//  Calculator.swift
//  Calculator
//
//  Created by Dev on 10/26/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class Calculator {

    static func sum(a: Double, b: Double) -> Double {
        print(String(a) + String(b))
        return a + b
    }
    
    static func res(a: Double, b: Double) -> Double {
        return a - b
    }
    
    static func div(a: Double, b: Double) -> Double {
        return a / b
    }
    
    static func mult(a: Double, b: Double) -> Double {
        return a * b
    }
    
    static func inverse(a: Double) -> Double {
        return -a
    }
    
    static func percentage(a: Double, b: Double) -> Double {
        return (a / 100) * b
    }
}
