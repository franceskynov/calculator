//
//  Util.swift
//  Calculator
//
//  Created by Dev on 10/26/18.
//  Copyright © 2018 hightech-corp. All rights reserved.
//

import UIKit

class Util {
    
    static func toNumbr(l: UILabel) -> Double {
        if let ll = l.text, let nl = Double(ll) {
            return nl
        }
        
        return 0.0
    }
    
    static func to_s(n: Double) -> String {
        return String(n)
    }
}
